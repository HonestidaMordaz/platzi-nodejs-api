# Anotame.la

Web Services
-API = Application Public Interface
-REST = Representational State Transfer (of resources)
- Hypermedia APIs

Resources
- User
- Company
- Files

Entities
- Freddy Vega
- Platzi
- Objects

Representation
- json
- xml
- csv
- text
- BLOB

VERBS (CRUD)
- POST (CREATE)
- GET (RETREIVE)
- PUT (UPDATE)
- DELETE (DELETE)

POST /user
GET /user/id
PUT /user/id
DELETE /user/id

## Detalles

API REST
- express
- mocha -> testing framework
- persistencia -> en memoria
- BDD -> Behavioral Driven Development