# API REST de Anotame.la
Anotamela es una aplicación que permite crear notas de código.

## Métodos HTTP permitidos

|  Método  |  Descripción                           |
| -------- | -------------------------------------- |
| 'GET'    | Obtener un recurso o lista de recursos |
| 'POST'   | Crear un recurso                       |
| 'PUT'    | Actualizar un recurso                  |
| 'DELETE' | Eliminar un recurso                    |

## Código de respuesta

|  Código  |  Descripción                                       |
| -------- | -------------------------------------------------- |
| '200'    | Sucess                                             |
| '201'    | Success - Nuevo recurso creado                     |
| '204'    | Success - No hay contenido para responder          |
| '400'    | Bad Request - i.e. Su solicitud no se pudo evaluar |
| '401'    | Unauthorized - Usuario no tiene autorización       |
| '404'    | Not Found - Recurso no existe                      |
| '422'    | Unprocessable Entity - i.e. Errores de validación  |
| '429'    | Limite de uso excedido, intenta más tarde          |
| '500'    | Internal Error - Error de servidor                 |
| '503'    | Forbiden - Servicio no disponible                  |

## Crear una nota nueva
	Solicitud [POST] /notas

		{
			{
				"title" : "Platzi node-pro",
				"description" : "Introducción a clase",
				"type" : "js",
				"body" : "Mira mamá, ya se JS"
			}
		}

	Respuesta
		{
			"nota" : {
				"id" : "123",
				"title" : "Platzi node-pro",
				"description" : "Introducción a clase",
				"type" : "js",
				"body" : "Mira mamá, ya se JS"
			}
		}

## Obtener una nota
	Solicitud GET /notas/123

	Respuesta
		{
			"nota" : {
				"id" : "123",
				"title" : "Platzi node-pro",
				"description" : "Introducción a clase",
				"type" : "js",
				"body" : "Mira mamá, ya se JS"
			}
		}

## Actualizar una nota
	Solicitud PUT /nota/123

		{
			"nota" : {
				"title" : "Platzi node-pro",
				"description" : "Introducción a clase",
				"type" : "ruby",
				"body" : "Mira mamá, ya se ruby"
			}
		}

	Respuesta

		{
			"nota" : {
				"id" : 123,
				"title" : "Platzi node-pro",
				"description" : "Introducción a clase",
				"type" : "ruby",
				"body" : "Mira mamá, ya se ruby"
			}
		}

## Eliminar una nota
	Solicitud DELETE /nota/123

		{
			"nota" : {
				"id" : 123,
				"title" : "Platzi node-pro",
				"description" : "Introducción a clase",
				"type" : "ruby",
				"body" : "Mira mamá, ya se ruby"
			}
		}