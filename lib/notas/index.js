/*
 * Dependencies
 */
var app = require('express')()
var logger = require('../logger')
var _ = require('lodash')

/*
 * Locals
 */
var db = {}

/*
 * HTTP VERBS
 */
app.get('/notas', function (req, res) {
	var notas = _.values(db)

	res
		.status(200)
		.set('Content-Type', 'application/json')
		.json({
			notas : notas
		})
})

app
	.route('/notas/:id?')
	/*
	 * Universal route handler
	 */
	.all(function (req, res, next) {
		logger.info(req.method, req.path, req.body)
		res.set('Content-Type', 'application/json')
		next()
	})

	/*
	 + POST route handler
	 */
	.post(function (req, res) {
		var notaNueva = req.body.nota
		notaNueva.id = Date.now()

		db[notaNueva.id] = notaNueva

		res
			.status(201)
			.json({
				nota : db[notaNueva.id]
			})
	})

/*
 * GET route handlers
 */
	.get(function (req, res, next) {
		var id = req.params.id

		if (!id) {
			console.log('no param')
			return next()
		}

		var nota = db[id]

		if (!nota) {
			return res
				.status(404)
				.send({})
		}

		res.json({
			notas : nota
		})
	})

	/*
	 + PUT route handlers
	 */
	 .put(function (req, res, next) {
	 	var id = req.params.id

	 	if (!id) {
	 		return next()
	 	}

	 	var notaActualizada = req.body.nota
	 	notaActualizada.id = parseInt(id, 10)

	 	db[id] = notaActualizada

	 	res
	 		.json({
	 			nota : [db[id]]
	 		})
	 })

	 /*
	  * DELETE route handler
	  */
	 .delete(function (req, res) {
	 	var id = req.params.id

	 	if (!id) {
	 		return next()
	 	}

	 	delete db[id]

	 	res
	 		.status(204)
	 		.send()
	 })

module.exports = app