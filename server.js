/*
 * Dependencies
 */
var express = require('express')
var logger = require('./lib/logger')
var bodyParser = require('body-parser')
var cors = require('cors')
var mongoose = require('mongoose')
/*
 * Locals
 */
var app = module.exports = express()
var port = process.env.PORT || 3000

/*
 * Parse JSON requests
 */
app.use(bodyParser.json('application/json'))
app.use(cors())

/*
 * Route handlers
 */
var notas = require('./lib/notas')
app.use(notas)

/*
 * Start server if it is not a module
 */
if (!module.parent) {
	app.listen(port, onListenning)
}

/*
 * Helpers
 */
function onListenning () {
	logger.info('Anotamel.a API listening on localhostd:%s', port)
}