var request = require('supertest-as-promised')
var api = require('../../server.js')

var host = 'http://localhost/4200/api'
request = request(host)

module.exports.createNote = function createNode (note) {
	console.log('CreateNOte')

	var sample = {
		"nota" : {
			"title" : "Platzi #node-pro",
			"description" : "Introduccion a clase",
			"type" : "js",
			"body" : "Soy el cuerpo de JSON"
		}
	}

	note = note || sample

	return request
		.post('/notas')
		.set('Accept', 'application/json')
		.send(note)
		.expect(201)
		.expect('Content-Type', /application\/json/)
		.then(function getNote(res) {
			this.note = res.body.nota
			this.id = res.body.nota.id
		}.bind(this))
}