/*
 * Dependencies
 */
var request = require('supertest-as-promised')
var api = require('../server.js')
var host = process.env.API_TEST_HOST || api
var mongoose = require('mongoose')
var _ = require('lodash')

request = request(host)

/*
 * Pruebas de comportamiento
 */
describe('Coleccion de Notas [/notas]', function () {
	before(function (done) {
		mongoose.connect('mongodb://localhost/anotamela.test', done)
	})

	after(function (done) {
		mongoose.disconnect(done)
		mongoose.models = {}
	})

	/*
	 * Pruebas para el metodo POST
	 */
	describe('POST', function () {
		it('Debería crear una nota nueva', function (done) {
			var data = {
				"title" : "Platzi #node-pro",
				"description" : "Introducción a calse",
				"type" : "js",
				"body" : "Soy el cuerpo de JSON"
			}

			request
				.post('/notas')
				.set('Accept', /application\/json/)
				.send(data)
			  .expect(201)
			  .expect('Content-Type', /application\/json/)
			  .end(function (err, res) {
			  	var nota
			  	var body = res.body

			  	console.log('body:', body)

			  	expect(body).to.have.property('nota')
			  	nota = body.nota

			  	expect(nota).to.have.property('title', 'Platzi #node-pro')
			  	expect(nota).to.have.property('description', 'Introducción a calse')
			  	expect(nota).to.have.property('type', 'js')
			  	expect(nota).to.have.property('body', 'Soy el cuerpo de JSON')
			  	expect(nota).to.have.property('id')

			  	done(err)
			  })
		})
	})

	/*
	 * Pruebas para el método GET
	 */
	describe('GET /notas/:id', function() {
		it('Deberia obtener una nota existente.', function (done) {
			var id
			var data = {
					"nota" : {
						"title" : "Platzi #node-pro",
						"description" : "Introducción a calse",
						"type" : "js",
						"body" : "Soy el cuerpo de JSON"
					}
			}

			request
				.post('/notas')
				.set('Accept', 'application/json')
				.send(data)
				.expect(201)
				.expect('Content-Type', /application\/json/)
				.then(function getNota (res) {
					id = res.body.nota.id

					return request
						.get('/notas/' + id)
						.send()
						.expect(200)
						.expect('Content-Type', /application\/json/)
				}, done)
				.then(function assertions(res) {
					var nota
					var body = res.body

					expect(body).to.have.property('notas')
					nota = body.notas

					expect(nota).to.have.property('id', id)
					expect(nota).to.have.property('title', 'Platzi #node-pro')
					expect(nota).to.have.property('description', 'Introducción a calse')
					expect(nota).to.have.property('type', 'js')
					expect(nota).to.have.property('body', 'Soy el cuerpo de JSON')
					done()
				}, done)
		})
	})

	/*
	 * Pruebas para el método PUTS
	 */
	describe('PUT', function () {
		it('Debería actualizar una nota existente', function (done) {
			var id
			var data = {
				"nota": {
					"title": "Platzi #node-pro",
					"description": "Introduccion a clase",
					"type": "js",
					"body": "Soy el cuerpo de JSON"
				}
			}

			request
				.post('/notas')
				.set('Accept', 'application/json')
				.send(data)
				.expect(201)
				.expect('Content-Type', /application\/json/)
				.then(function getNota (res) {
					var update = {
						"nota": {
							"id" : res.body.nota.id,
							"title": "Platzi #node-pro",
							"description": "Introduccion a clase",
							"type": "ruby",
							"body": "Soy el cuerpo de ruby"
						}
					}

					id = res.body.nota.id

					return request.put('/notas/' + id)
						.set('Accept', 'application/json')
						.send(update)
						.expect(200)
						.expect('Content-Type', /application\/json/)
				}, done)
				.then(function assertions (res) {
					var nota
					var body = res.body

					expect(body).to.have.property('nota')
					expect(body.nota).to.be.an('array')
						.and.to.have.length(1)

					nota = body.nota[0]

					expect(nota).to.have.property('id', id)
					expect(nota).to.have.property('title', 'Platzi #node-pro')
					expect(nota).to.have.property('description', 'Introduccion a clase')
					expect(nota).to.have.property('type', 'ruby')
					expect(nota).to.have.property('body', 'Soy el cuerpo de ruby')
					done()
				}, done)
		})
	})

	/*
	 + Pruebas para el metodo DELETE
	 */
	describe('DELETE', function () {
		it('Debería eliminar una nota existente', function (done) {
			var id
			var data = {
				"nota": {
					"title": "Platzi #node-pro",
					"description": "Introduccion a clase",
					"type": "js",
					"body": "Soy el cuerpo de JSON"
				}
			}

			request
				.post('/notas')
				.set('Accept', 'application/json')
				.send(data)
				.expect(201)
				.expect('Content-Type', /application\/json/)
				.then(function deleteNota (res) {
					id = res.body.nota.id

					return request
						.delete('/notas/' + id)
						.set('Accept', 'application/json')
						.expect(204)
				}, done)
				.then(function assertions(res) {
					var nota
					var body = res.body

					expect(body).to.be.empty

					return request
						.get('/notas/' + id)
						.set('Accept', 'application/json')
						.send()					
						.expect(400)
				}, done)
				.then(function confirmation (res) {
					var body = res.body
					expect(body).to.be.empty()
					done()
				}, done)
		})
	})

	describe('GET /notas', function () {
		it('Debería obtener todas las notas existentes.', function (done) {
			var id1
			var id2

			var data1 = {
				"nota" : {
					"title" : "Platzi #node-pro",
					"description" : "Introduccion a clase",
					"type" : "js",
					"body" : "Soy el cuerpo del JSON"
				}
			}

			var data2 = {
				"nota" : {
						"title" : "Platzi #node-pro",
						"description" : "Ya casi",
						"type" : "ruby",
						"body" : "Whooo!"
				}
			}

			request
				.post('/notas')
				.set('Accept', 'application/json')
				.send(data1)
				.expect(201)
				.expect('Content-Type', /application\/json/)
				.then(function createAnotherNota(res) {
					id1 = res.body.nota.id

					return request
						.post('/notas')
						.set('Accept', 'application/json')
						.send(data2)
						.expect(201)
						.expect('Content-Type', /application\/json/)
				})
				.then(function getNotas(res) {
					id2 = res.body.nota.id

					return request
						.get('/notas')
						.set('Accept', 'application/json')
						.expect(200)
						.expect('Content-Type', /application\/json/)
				}, done)
				.then(function assertions(res) {
					var body = res.body

					expect(body).to.have.property('notas')
					expect(body).to.be.an('array')
						.and.to.have.length(2)

					var notas = body.notas
					var nota1 = _.find(notas, { id : id1 })
					var nota2 = _.find(notas, { id : id2 })

					expect(nota1).to.have.property('id', id1);
					expect(nota1).to.have.property('title', 'Platzi #node-pro');
					expect(nota1).to.have.property('description', 'Introduccion a clase');
					expect(nota1).to.have.property('type', 'js');
					expect(nota1).to.have.property('body', 'Soy el cuerpo de JSON');

					expect(nota2).to.have.property('id', id2);
					expect(nota2).to.have.property('title', 'Mejorando.la #node-pro');
					expect(nota2).to.have.property('description', 'Ya casi');
					expect(nota2).to.have.property('type', 'ruby');
					expect(nota2).to.have.property('body', 'Whooo!')

					done()
				}, done)
		})
	})
})